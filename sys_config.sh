#!/bin/bash

#This script configures a new Debian Linux system to my requirements.
#This script contains:
# 1) Active script to be executed.
# 2) Notes on how to complete tasks that must be performed manually
#     (such as inputing credential values in ~/.bashrc)


# Display a warning message to the user
#===================================================================
echo "----------"
echo "This script configures a new Debian Linux system. It will
install many programs and will add lines to .bashrc and other 
important files. Do you wish to continue?"
echo "----------"
echo "Type any keys if you wish to alter this system:"
# Read user input into a variable
read userInput
# Now you can use the user input in your script
echo "You agreed to continue by typing any keys: $userInput"


#Is this a laptop/desktop setup
#===================================================================
echo "Is this a desktop or laptop Linux? (y/n)"
echo "(The alternative is a server linux)"
read linuxInstallType


#Written instructions
#===================================================================


#Installing programs
#===================================================================
#All changes to Linode default Linux:
#These changes are first applied to user 'root', then will need to be applied to mbogonov02
#create a directory named .ssh in the home folder if it doesn't already exist
mkdir ~/.ssh
#create a file named authorized_keys in that file, and place the contents of id_rsa.pub (from your local machine into that file)
touch ~/.ssh/authorized_keys
#Create a limited user:
#accept all the defaults
adduser mbogonov03
#and then...
adduser mbogonov03 sudo
#HERE...

# Check if the user input is "y"
if [ "$linuxInstallType" = "y" ]; then
    # Run code for laptop or desktop setup
    echo "Running laptop or desktop installs..."
    # Insert your code here
else
    # Code is skipped
    echo "No laptop or desktop installs..."
fi


#Altering variables in ~/.bashrc
#===================================================================
#Search and replace HISTSIZE AND HISTFILESIZE
#HISTSIZE=50000 #[old value]
#HISTFILESIZE=100000 #[old value]
#The -i parameter instructs sed to search "in place"
sed -i "s/HISTSIZE=/HISTSIZE=50000 #/" ~/.bashrc
sed -i "s/HISTFILESIZE=/HISTFILESIZE=100000 #/" ~/.bashrc


#Adding aliases and keys to ~/.bashrc
#===================================================================
cat << EOF >> ~/.bashrc

#CUSTOM BASH ALIASES
export MYTESTENVVAR3000='BLAH934gsdgwHH'
#https://unix.stackexchange.com/questions/664311/current-date-in-cal-is-not-highlighted-in-recent-debian
alias cal="if [ -t 1 ] ; then ncal -b ; else /usr/bin/cal ; fi"
EOF

# Check if the user input is "y"
if [ "$linuxInstallType" = "y" ]; then
    # Run code for laptop or desktop setup
    echo "Make laptop or desktop .bashrc changes..."
    # Insert your code here
cat << EOF >> ~/.bashrc

#CUSTOM BASH ALIASES
alias open=xdg-open
alias sus="systemctl suspend"
export MYTESTENVVAR3000='BLAH934gsdgwHH'
EOF
else
    # Code is skipped
    echo "No laptop or desktop changes to .bashrc..."
fi

#Run source ~/.bashrc to update these changes
source ~/.bashrc





















