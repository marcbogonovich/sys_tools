#!/usr/bin/python

#This script downloads a YT video for watching offline.
#I prepared this for my flight to Hong Kong in January 2024.

#I followed the following video tutorial:
#Internet Made Coder:
#https://youtu.be/vEQ8CXFWLZU?si=5Z08RiBV0ko9pFCj&t=320
#pytube documentation:
#https://pytube.io/en/


#LIBRARIES
#===================================================================
from pytube import YouTube
#If I wish to make this a command line tool:
from sys import argv
from time import sleep
import os


#COMMAND LINE DOWNLOADER TOOL
#===================================================================
#UNFINISHED, PUT INTO OTHER SCRIPT called ytdl_terminal.py
#Place this line into ~/.bashrc
#alias ytdl='python3 ~/Desktop/Gitlab/sys_tools/YT_dler/ytdl_terminal.py'
#This program only dls one link per command.
#argv[0] is the name of the program itself
#program_name = argv[0]
#link = argv[1]
#yt = YouTube(link)
#Progress reporting a YT link
#print("Running ", program_name)
#print("Title: ", yt.title)
#print("Views: ", yt.views)
#sleep(1)


#LINK LIST DOWNLOADER
#===================================================================
#Create a list of links to download
links = [
"https://www.youtube.com/watch?v=x1TqLcz_ug0",
"https://www.youtube.com/watch?v=vkFNSWVVxO0",
"https://www.youtube.com/watch?v=HOHRHZ6Cv_A",
"https://www.youtube.com/watch?v=mrjq3lFz23s"
]

try:

  for link in links:
    yt = YouTube(link)
    #Progress reporting a YT link
    print("----------")
    print("Downloading: ")
    print("Title: ", yt.title)
    print("Views: ", yt.views)
    print(link)
    print("----------")
    sleep(0.1)
    #https://stackoverflow.com/questions/2057045/os-makedirs-doesnt-understand-in-my-path
    video_path = os.path.expanduser('~/Videos')
    #yd = yt.streams.get_highest_resolution()
    yd = yt.streams.get_lowest_resolution()
    yd.download(video_path)

except Exception as e:
    print("An error occurred:", str(e))



















