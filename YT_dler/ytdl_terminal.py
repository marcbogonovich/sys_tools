#!/usr/bin/python

#THIS IS THE TERMINAL PROGRAM VERSION
#This dls only one video at a time at the moment
#This script downloads a YT video for watching offline.
#I prepared this for my flight to Hong Kong in January 2024.

#I followed the following video tutorial:
#Internet Made Coder:
#https://youtu.be/vEQ8CXFWLZU?si=5Z08RiBV0ko9pFCj&t=320
#pytube documentation:
#https://pytube.io/en/


#LIBRARIES
#===================================================================
from pytube import YouTube
#If I wish to make this a command line tool:
from sys import argv
from time import sleep
import os


#COMMAND LINE DOWNLOADER TOOL
#===================================================================
#Place this line into ~/.bashrc
#alias ytdl='python3 ~/Desktop/Gitlab/sys_tools/YT_dler/ytdl_terminal.py'
#This program only dls one link per command.
#argv[0] is the name of the program itself
program_name = argv[0]
link = argv[1]
yt = YouTube(link)
#Progress reporting a YT link
print("----------")
print("Downloading: ")
print("Title: ", yt.title)
print("Views: ", yt.views)
print(link)
sleep(0.1)
print("----------")
#https://stackoverflow.com/questions/2057045/os-makedirs-doesnt-understand-in-my-path
video_path = os.path.expanduser('~/Videos')
yd = yt.streams.get_highest_resolution()
#yd = yt.streams.get_lowest_resolution()
yd.download(video_path)




























