#!/usr/bin/python3

#This script was originally created in the Klarity repository under 
#Sandbox/emailer.py and Sandbox/emailer_loop.py
#a copy of the former is now in this repository
#I first started consulting the following tutorial, before realizing I've 
#already done all of this before in the klarity repo:
#https://www.geeksforgeeks.org/send-mail-gmail-account-using-python/


#LIBRARIES
#===================================================================
import smtplib
import ssl
from email.message import EmailMessage
import os
#Needed to allow the script to sleep between emails:
import random
import time
#Needed for the for loop to iterate through three lists in parallel
import itertools


#EMAIL CREDENTIALS, TARGETS (EMAILS, NAMES, UNIQUE BODY MATERIAL)
#===================================================================
# Define email sender and receiver
mbogonov_gmail          = os.environ.get('MBOGONOV_GMAIL')
#We do not actually use the real mbogonov gmail password
#Rather we use the specificly designed app password
#mbogonov_gmail_password = os.environ.get('MBOGONOV_GMAIL_PASSWORD')
mbogonov_gmail_password = os.environ.get('MBOGONOV_GMAIL_APP_PASSWORD_PYTHON3')
marcbogonovich_gmail    = os.environ.get('MARCBOGONOVICH_GMAIL')
#Place these environment variables into their appropriate places
email_sender   = mbogonov_gmail
email_password = mbogonov_gmail_password #Note this is the app password, not password 
email_receiver = marcbogonovich_gmail
#TARGET NAMES, DEPARTMENTS, BODY MATERIAL
target_emails         = [marcbogonovich_gmail, "marc.bogonovich+1@gmail.com", "davidbogonovich@yahoo.com"]
target_names          = ["Name_01","Name_02","Name_03"]
target_departments    = ["Department of Things and Stuff", "Department of Norway Sniffing", "School of Sodoku and Connections"]
field_As              = ["Field_A1, Field_A2", "Field_A1", "Field_A99"]
field_Bs              = ["Field_A63", "related fields", "related fields"]
target_cities         = ["Shenzhen", "Shenzhen", "Shenzhen"]


target_emails = [
"international@szu.edu.cn",
"cjshang@szu.edu.cn",
"lihui80@szu.edu.cn",
"xingzhi.xu@szu.edu.cn",
"ruoranli@szu.edu.cn",
"5892314@qq.com",
"mrzhao@szu.edu.cn",
"zxzheng@szu.edu.cn",
"estella@szu.edu.cn",
"lcl@szu.edu.cn",
"lac@szu.edu.cn",
"liyezou123@gmail.com",
"jrh_psy@163.com"
]

#target_emails = [
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com",
#"marc.bogonovich@gmail.com"
#]

#target_emails = [
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com",
#"beta@openwords.com"
#]

target_names = [
"International Office",
"Shang Chenjing",
"Vice Dean Prof. Hui Li",
"Vice Dean Prof. Xu Xingzhi",
"Li Ruoran",
"Peng Aifang",
"Prof. Zhao Mingren",
"Dean Prof. Zheng Zunxin",
"Qu Tao",
"Vice Dean Prof. Hou Lingling",
"Liao Aichun",
"Prof. Zou Liye",
"Jiang Ronghuan"
]

target_departments = [
"College of International Exchange",
"College of Life Sciences and Oceanography",
"College of Life Sciences and Oceanography",
"Medical School",
"Medical School",
"Faculty of Education",
"Faculty of Education",
"College of Economics",
"College of Economics",
"Law School",
"Law School",
"School of Psychology",
"School of Psychology"
]

field_As = [
"the social sciences",
"the biological sciences",
"the biological sciences",
"medical research",
"medical research",
"the field of education",
"the field of education",
"special economic zone management",
"special economic zone management",
"law",
"law",
"psychology",
"psychology"
]

field_Bs = [
"the natural sciences",
"related fields",
"related fields",
"related fields",
"related fields",
"related fields",
"related fields",
"related fields in economics",
"related fields in economics",
"related fields",
"related fields",
"related fields",
"related fields"
]

target_cities = [
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen",
"Shenzhen"
]


#EMAIL FOR LOOP
#===================================================================
#Looping through several lists in parallel:
#https://www.geeksforgeeks.org/python-iterate-multiple-lists-simultaneously/
for (recipient_email, name, department, field_A, field_B, city) in zip(target_emails, target_names, target_departments, field_As, field_Bs, target_cities):
  print (recipient_email, name, department, field_A, field_B, city, sep=",")
  #This will make the program sleep a little bit between emails:
  #-------------------------------------------
  x = random.random()
  x = x * 10 + 5
  x = round(x, 3)
  print("Waiting ", x, " seconds before sending the next email.")
  time.sleep(x)
  #Reporting on the email progress:
  #-------------------------------------------
  #Inserting variables into strings with triple quotes (basically just use f string format):
  #https://stackoverflow.com/questions/3877623/can-you-have-variables-within-triple-quotes-if-so-how
  #https://12ft.io/proxy?q=https%3A%2F%2Frealpython.com%2Fpython-f-strings%2F
  #TEST
  #recipient = "HIYA"
  #company   = "ACME"
  t = time.localtime()
  current_time = time.strftime("%H:%M:%S", t)
  #END TEST
  # Set the subject and body of the email
  subject = f'The {department} and English language editing'
  body = f"""
{name},

I am writing to you to learn more about how I can help the scholars in the {department} with their English language writing. I am a scientist from the United States living in Hong Kong, China and I run an English language editing group called Oceaneditors.com

I would be happy to travel to your location in {city}. This would give me the opportunity to interact with your personnel and allow me to demonstrate our services to your department in person.

What do we do? Chinese scholars hand us their manuscripts and our editors correct the English in these manuscripts. In other words, we help prepare manuscripts for publication in English language journals. For the {department} I imagine you would have needs in {field_A} and {field_B}. Our editors are experts with relevant advanced degrees (Ph.D., J.D. etc.).

I would be very happy to answer any questions that you may have.

Best regards,
Marc Bogonovich, Ph.D.
Senior Editor
OceanEditors.com
  """
  
  em = EmailMessage()
  em['From'] = email_sender
  em['To'] = recipient_email #email_receiver
  em['Subject'] = subject
  em.set_content(body)
  
  # Add SSL (layer of security)
  context = ssl.create_default_context()
  
  # Log in and send the email
  # One puzzling behavior is that this is sending a bcc to the email in 
  # marcbogonovich_gmail whenever I am not explicitly sending to that same email. 
  with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as smtp:
    smtp.login(email_sender, email_password)
    smtp.sendmail(email_sender, email_receiver, em.as_string())



#In other words, we help prepare manuscripts for publication in English language journals. We perform editing services for a wide range of subjects. 


#School of Marxism,

#I am writing to you to learn more about how I can help the scholars in the School of Marxism with their English language writing. I am a scientist from the United States living in Hong Kong, China and I run an English language editing group called Oceaneditors.com

#I am personally interested in the School of Marxism because I have strong interests in socialism and communism and I am quite eager to learn more about Chinese Marxism in the academic sector. Our group (OceanEditors) is an experiment in enterprise governance, which I believe is of great interest to left politics and research. Specifically, we are an E2C company (Exit to Community). This means that we have decided that when OceanEditors is exited from private ownership it will convert to a worker cooperative or public directed and owned organization.

#What do we do? Chinese scholars hand us their manuscripts and our editors correct the English in these manuscripts. In other words, we help prepare manuscripts for publication in English language journals. Our editors all have advanced degrees in the relevant subject matter.

#I am a resident of Hong Kong, China and I would be happy to travel to your location in Shenzhen. This would give me the opportunity to interact with your personnel and allow me to demonstrate our services to your department in person.

#I would be very happy to answer any questions that you may have.

#Thank you very much,
#Marc Bogonovich, Ph.D.
#Senior Editor
#OceanEditors.com




#And here is another paragraph with {current_time}.




















