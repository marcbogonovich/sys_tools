#!/usr/bin/bash

#This script creates a virtual machine. It follows this tutorial:
#https://www.youtube.com/watch?v=9ldC03Mqb0I
#This bios line is probably not going to work as his system is a Fedora 29.
#He says that if you do not specify it, that it will default to cbios.
#Which is ok I guess?
#I'll try this with and without that line.
#This is installing a win10.iso file, from the same directory
#and it is being installed from an emulated cdrom? ok.

#Prior to running this script, you need to run the following command within the same directory? Thus creating vm1.img which is the hard drive you'll be running the machine on? I believe?:
#qemu-img create -f qcow2 vm1.img 35G

echo "Creating Machine..."

#Specify system, memory, number of cores, threads
#qemu-system-x86_64 --enable-kvm -m 16384 -cpu host,kvm=off \
#-M q35 \
#-smp cores=2,threads=2\
#-bios /usr/share/edk2/ovmf/OVMF_CODE.fd \
#-rtc base=localtime \
#-cdrom win10.iso \
#-hda vm1.img \
#-usb -device usb-tablet \
#-boot d \

#Specify system, memory, number of cores, threads
#removed the bios line:
qemu-system-x86_64 --enable-kvm -m 16384 -cpu host,kvm=off \
-M q35 \
-smp cores=2,threads=2\
-rtc base=localtime \
-cdrom win10.iso \
-hda vm1.img \
-usb -device usb-tablet \
-boot d \













