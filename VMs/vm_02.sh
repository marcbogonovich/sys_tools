#!/usr/bin/bash

#This script creates a virtual machine.
#It follows a conversation with ChatGPT, and a tutorial:
#https://chat.openai.com/c/383e38c6-111c-4294-9b08-8a50fbfd4b57
#https://www.youtube.com/watch?v=9ldC03Mqb0I

#Prior to running this script, you can run the following command within the same directory, thus creating vm1.img which is the hard drive you'll be running the machine on? I believe?:
#qemu-img create -f qcow2 vm2.img 35G

#ChatGPT offered two alternative scripts, one that uses libvirt and one that uses only qemu. I'm leaning on trying the qemu script.
