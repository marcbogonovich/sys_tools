#!/bin/bash

#This script uses the sensors terminal program to collect data on the temperature
#of the local computer's hardware. The script then logs this data in a file named
#`~/Desktop/Gitlab/sys_tools` along with the Unix time.

#This script was created in consultation with ChatGPT:
#https://chatgpt.com/c/b2e60045-97e0-4d18-a1cb-de712cf0c831

#This script must be run using crontabs with the following settings.
#Set Up a Crontab:
#Run crontab -e to edit your crontab.
#Add the following line to run the script every five minutes:
#*/5 * * * * $HOME/Desktop/Gitlab/sys_tools/log_temperatures.sh

#Further, to be executable by crontabs, you must actually chmod this .sh file with
#the following command within the same directory as the file:
#chmod +x log_temperatures.sh


# Define the output file
#===================================================================
output_file="$HOME/Desktop/Gitlab/sys_tools/logfile_temperatures.csv"


# Check if the file exists; if not, create it and add the header
if [ ! -f "$output_file" ]; then
    echo "time_unix,Package_id_0,Core_0,Core_4,Core_8,Core_9,Core_10,Core_11,Core_12,Core_13,Core_14,Core_15" > "$output_file"
fi

# Get the current Unix time
current_time=$(date +%s)

# Get the temperature readings using sensors
sensors_output=$(sensors)

# Extract the specific temperatures you are interested in
package_temp=$(echo "$sensors_output" | grep 'Package id 0:' | awk '{print $4}' | tr -d '+°C')
core_0_temp=$(echo "$sensors_output" | grep 'Core 0:' | awk '{print $3}' | tr -d '+°C')
core_4_temp=$(echo "$sensors_output" | grep 'Core 4:' | awk '{print $3}' | tr -d '+°C')
core_8_temp=$(echo "$sensors_output" | grep 'Core 8:' | awk '{print $3}' | tr -d '+°C')
core_9_temp=$(echo "$sensors_output" | grep 'Core 9:' | awk '{print $3}' | tr -d '+°C')
core_10_temp=$(echo "$sensors_output" | grep 'Core 10:' | awk '{print $3}' | tr -d '+°C')
core_11_temp=$(echo "$sensors_output" | grep 'Core 11:' | awk '{print $3}' | tr -d '+°C')
core_12_temp=$(echo "$sensors_output" | grep 'Core 12:' | awk '{print $3}' | tr -d '+°C')
core_13_temp=$(echo "$sensors_output" | grep 'Core 13:' | awk '{print $3}' | tr -d '+°C')
core_14_temp=$(echo "$sensors_output" | grep 'Core 14:' | awk '{print $3}' | tr -d '+°C')
core_15_temp=$(echo "$sensors_output" | grep 'Core 15:' | awk '{print $3}' | tr -d '+°C')

# Append the data to the CSV file
echo "$current_time,$package_temp,$core_0_temp,$core_4_temp,$core_8_temp,$core_9_temp,$core_10_temp,$core_11_temp,$core_12_temp,$core_13_temp,$core_14_temp,$core_15_temp" >> "$output_file"



