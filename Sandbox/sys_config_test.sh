#!/bin/bash

#This script configures a new Debian Linux system to my requirements.


#Testing multi-line commands
#https://linuxhint.com/split-long-bash-command/
echo "Can this work\
because that'd be cool."


#The following section tests appending an line to a file.
# https://linuxhint.com/append-multiple-file-lines-bash/
touch test_file
printf "\nThis command is on\
 multiple \"script lines\"\
 but produces two lines in the document.\n\
 This is the second line in the \
 document." >> test_file
#Because bash script lines have complicated "quotations" with different kinds
#of 'quotations', an alternative method of placing bash script lines into a file
#must be tried that allows one to place a wide range of hairy code into a bash file.
#Heredoc may be that command:
#https://linuxhint.com/bash-heredoc-tutorial/
#https://www.cyberciti.biz/faq/using-heredoc-rediection-in-bash-shell-script-to-write-to-file/
#https://stackoverflow.com/questions/2953081/how-can-i-write-a-heredoc-to-a-file-in-bash-script
cat << EOF >> test_file
  
  "Testing'\n'"'IS THIS WORKING?
  another line
  another \nline
  another line
  echo "huh?"
EOF
#^It appears that printf does not work, whereas cat does?! Ok.

























