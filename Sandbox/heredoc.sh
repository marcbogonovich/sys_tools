#!/bin/bash

#This script explores the heredoc functionality in bash.
#A similar test script was also created in a different Gitlab repository:
#Gitlab/algo/Sandbox/bash_testing/heredoc_testing.sh

#Heredoc tutorial:
#https://linuxhint.com/bash-heredoc-tutorial/
