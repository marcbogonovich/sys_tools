#!/usr/bin/python3

#This script was originally created in the Klarity repository under 
#Sanbox/emailer.py

#This script will email a list of recipients (Their name, company, and email).
#The following tutorial/website was consulted to build this code:
#https://realpython.com/python-send-email/
#However, Google apparently deprecated the ability to lessen security to allow 3rd
#party services access to Google accounts. Hence, a new solution must be found:
#https://www.youtube.com/watch?v=g_j6ILT-X0k
#Code appears to be here:
#https://github.com/thepycoach/automation/blob/main/Send%20Emails.py


#Deprecated solution:
"""
#LIBRARIES
#===================================================================
import smtplib, ssl


smtp_server = "smtp.gmail.com"
port = 587  # For starttls
sender_email = "marc.bogonovich@gmail.com"
password = input("Type your password and press enter: ")

# Create a secure SSL context
context = ssl.create_default_context()

# Try to log in to server and send email
try:
    server = smtplib.SMTP(smtp_server,port)
    server.ehlo() # Can be omitted
    server.starttls(context=context) # Secure the connection
    server.ehlo() # Can be omitted
    server.login(sender_email, password)
    # TODO: Send email here
except Exception as e:
    # Print any error messages to stdout
    print(e)
finally:
    server.quit() 
"""

#New solution:
#LIBRARIES
#===================================================================
import smtplib
import ssl
from email.message import EmailMessage
import os

# Define email sender and receiver
#email_sender = 'write-email-here'
#email_password = 'write-password-here'
#email_receiver = 'write-email-receiver-here'
mbogonov_gmail          = os.environ.get('MBOGONOV_GMAIL')
#We do not actually use the real mbogonov gmail password
#Rather we use the specificly designed app password
#mbogonov_gmail_password = os.environ.get('MBOGONOV_GMAIL_PASSWORD')
mbogonov_gmail_password = os.environ.get('MBOGONOV_GMAIL_APP_PASSWORD_PYTHON3')
marcbogonovich_gmail    = os.environ.get('MARCBOGONOVICH_GMAIL')
#Place these environment variables into their appropriate places
email_sender   = mbogonov_gmail
email_password = mbogonov_gmail_password #Note this is the app password, not password 
email_receiver = marcbogonovich_gmail
target_emails  = [marcbogonovich_gmail, "marc.bogonovich+1@gmail.com"] 


# Set the subject and body of the email
subject = 'TESTING PYTHON EMAIL SCRIPT'
body = """
I just sent this email via my Python email script. '' <--Hopefully those lines..
do not cause issue. Newline here:
Newline yo.

Two newlines, and yeah, let's see what happens if we keep writing on and on and on and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and and so on.
"""

em = EmailMessage()
em['From'] = email_sender
em['To'] = email_receiver
em['Subject'] = subject
em.set_content(body)

# Add SSL (layer of security)
context = ssl.create_default_context()

# Log in and send the email
with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as smtp:
    smtp.login(email_sender, email_password)
    smtp.sendmail(email_sender, email_receiver, em.as_string())




