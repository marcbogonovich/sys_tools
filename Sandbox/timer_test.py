#!/usr/bin/python

#The purpose of this code is to create a timer that can be set from the terminal.

#How to play a sound from the terminal:
#https://unix.stackexchange.com/questions/681289/play-sound-when-command-finishes
#This is how you can play the sound in the terminal:
#paplay /usr/share/sounds/freedesktop/stereo/complete.oga
#The following ChatGPT conversation created the code for this timer.
#https://chat.openai.com/c/31c1c6ce-ba80-4f97-a2c6-d250e1ce7482

#This timer has an alias in ~/.bashrc
#As follows:
#alias timer='python /path/to/timer_test.py'
#My correction:
#alias timer='python3 /path/to/timer_test.py'
#Called as follows, where an integer parameter signifies minutes:
#timer 5

import sys
import time
import subprocess

def run_timer(minutes):
    seconds = minutes * 60
    time.sleep(seconds)
    sound_file = "/usr/share/sounds/freedesktop/stereo/complete.oga"
    subprocess.run(["paplay", sound_file])

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python timer_test.py <minutes>")
        sys.exit(1)

    try:
        minutes = int(sys.argv[1])
    except ValueError:
        print("Invalid input. Please enter a valid integer for minutes.")
        sys.exit(1)

    run_timer(minutes)



