#The following are a set of my personal aliases placed into ~/.bashrc
#To enable them for the first time, run
#source .bashrc

#My personal alias lines
alias sublime="/opt/sublime_text/sublime_text"
alias openpdf=xdg-open
alias open=xdg-open
alias light="xbacklight -set"
alias sus="systemctl suspend"
export MYTESTENVVAR3000='BLAH934gsdgwHH'
alias EST="TZ=EST date"
alias est="TZ=EST date"
alias UTC="TZ=UTC date"
alias utc="TZ=UTC date"
#https://www.linuxjournal.com/content/how-check-battery-status-using-linux-command-line
alias batterystats="upower -i `upower -e | grep 'BAT'`"
alias battery="upower -i $(upower -e | grep BAT) | grep --color=never -E 'state|to\ full|to\ empty|percentage'"
alias bat="upower -i $(upower -e | grep BAT) | grep --color=never -E 'state|to\ full|to\ empty|percentage'"
#Creating alias with variables
# https://stackoverflow.com/questions/941338/how-to-pass-command-line-arguments-to-a-shell-alias#22684652
#This function allows you to use the following to activate a virtual environment:
#venv virtual_environment_folder_name
#rather than the previous:
#source virtual_environment_folder_name/bin/activate
alias blah='function _blah(){ echo "First: $1"; echo "Second: $2"; };_blah'
alias venv='function _venv(){ source $1/bin/activate; };_venv'
#Google Translate (THIS HAS NOT WORKED)
#From commandlinefu.com
#Usage: translate <phrase> <source-language> <output-language>
#Example:
#translate hello en es
#See this for a list of language codes:
#http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes 
# This needs to be turned into an alias:
# translate(){ wget -qO- "http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=$1&langpair=$2|${3:-en}" | sed 's/.*"translatedText":"\([^"]*\)".*}/\1\n/'; }
alias lsx='ls -lhGpt --color=always'
#Checks Python versions installed on system. From this link:
#https://stackoverflow.com/questions/30464980/how-to-check-all-versions-of-python-installed-on-osx-and-centos
alias pythons="ls -1 /usr/bin/python* | grep '.*[2-3]\(.[0-9]\+\)\?$'"
#View and adjust screen resolution with xrandr
#https://www.youtube.com/watch?v=jGGdDDv_XJo
alias screen="xrandr"
alias screenrestore="xrandr -s 0"
#How to add a new screen resolution and issue those command each boot up
#https://www.youtube.com/watch?v=ZVb5yWKdgvg
#Fixing the cal function:
#https://unix.stackexchange.com/questions/664311/current-date-in-cal-is-not-highlighted-in-recent-debian
alias cal="if [ -t 1 ] ; then ncal -b ; else /usr/bin/cal ; fi"
#neofetch is really a sysinfo command, and would better be sysinfo:
alias sysinfo="neofetch"
alias filecount="ls -1 | wc -l"
alias filecountall="la -1 | wc -l"
#LOCAL MACHINE COMMANDS
alias timer='python3 ~/Desktop/Gitlab/sys_tools/Sandbox/timer_test.py'
alias ytdl='python3 ~/Desktop/Gitlab/sys_tools/YT_dler/ytdl_terminal.py'
alias unix='echo $(date +%s)'
alias Unix='echo $(date +%s)'
alias UNIX='echo $(date +%s)'
alias 1='cd ../'
alias 2='cd ../../'
alias 3='cd ../../../'
alias 4='cd ../../../../'
alias 5='cd ../../../../../'
alias 6='cd ../../../../../../'
alias 7='cd ../../../../../../../'
alias 8='cd ../../../../../../../../'


# The following were bundled with my default .bashrc file 
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


# The following is a custom path added to the PATH variable. My understanding is that it enables
# programs within the folder to be used as functions that can be executed from the terminal.
# At the very least the functions can be executed by navigating into said folder.
# A custom path for a installing Python versions. This program is described in this video:
# https://www.youtube.com/watch?v=S4HfueSI-ow
# https://www.python.org/ftp/python/
export PATH=$PATH:~/Desktop/Gitlab/python-scripts
