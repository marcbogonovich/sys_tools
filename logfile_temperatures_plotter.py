#!/user/bin/python

#This script plots temperature from my machine, which is continually logged and stored in sys_tools/logfile_temperatures.csv

#This code was written by ChatGPT in this conversation:
#https://chatgpt.com/c/671f95c6-9640-800a-85a6-d99aad259843


import pandas as pd
import matplotlib.pyplot as plt

# Load the CSV data into a DataFrame
data = pd.read_csv('logfile_temperatures.csv')

# Convert Unix time to a more readable datetime format for the x-axis
data['time_unix'] = pd.to_datetime(data['time_unix'], unit='s')

# Plot Package_id_0 temperature over time
plt.plot(data['time_unix'], data['Package_id_0'], label='Package_id_0')

# Set plot labels and title
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.title('Package_id_0 Temperature Over Time')
plt.legend()

# Show the plot
plt.show()

